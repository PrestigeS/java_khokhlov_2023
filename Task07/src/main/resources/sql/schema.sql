drop table if exists product;

-- создание таблицы
create table product
(
    id bigserial primary key,
    name varchar(20),
    color varchar(20),
    quantity bigint,
    producer varchar(30)
);

