package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.repositories.ProductsRepositoryNamedParameterJdbcTemplateImpl;

import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        ProductsRepository productsRepository = new ProductsRepositoryNamedParameterJdbcTemplateImpl(dataSource);


        //test findById and save
        System.out.println("Test find by id (and save):");
        Product productTest = new Product(1L,"Chocolate", 13, "brown", "AlpenGold");
        productsRepository.save(productTest);
        System.out.println(productsRepository.findById(1L));

        //test deleteById
        System.out.println("delete by id:");
        productsRepository.save(new Product("sneakers", 27, "white", "Adidas"));
        System.out.println("before: " + productsRepository.findById(2L));
        productsRepository.deleteById(2L);
        System.out.println("after: " + productsRepository.findById(2L));

        //test updateById
        System.out.println("test update by id:");
        System.out.println("before: " + productsRepository.findById(1L));
        Product productUpdate = new Product(1L,"sneakers", 27, "black", "Nike");
        productsRepository.updateById(productUpdate);
        System.out.println("after: " + productsRepository.findById(1L));

        //test findAll
        System.out.println("test find all: ");
        productsRepository.save(Product.builder().name("phone").quantity(140).color("red").producer("Realme").build());
        System.out.println(productsRepository.findAll());

        //test findAllByQuantityGreaterThanOrderByIdDesc
        productsRepository.save(Product.builder().name("phone").quantity(17).color("green").producer("Honor").build());
        System.out.println("test quantity more than 25: ");
        System.out.println(productsRepository.findAllByQuantityGreaterThanOrderByIdDesc(25));

    }
}
