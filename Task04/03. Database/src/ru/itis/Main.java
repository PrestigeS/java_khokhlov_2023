package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);

        Student student1 = new Student("Марсель","Сидиков",28);
        studentsRepository.save(student1);
        Student student2 = new Student("Will","Smith",53);
        studentsRepository.save(student2);
        System.out.println("Добавились студенты Марсель и Will: ");
        System.out.println(studentsRepository.findAll());
        System.out.println("Поиск по id = 1: ");
        System.out.println(studentsRepository.findById(1L));
    }
}
