package classes;


public class Main {
    public static void main(String[] args){
        System.out.println(SqlGenerator.createTable(User.class));

        User testUser = new User();
        testUser.setFirstName("Will");
        testUser.setLastName("Smith");
        testUser.setWorker(true);
        System.out.println(SqlGenerator.insert(testUser));
    }
}
