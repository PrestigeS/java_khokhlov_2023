package classes;

import annotations.ColumnName;
import annotations.TableName;

import java.lang.reflect.Field;
import java.util.Date;

public class SqlGenerator {

    private static String createTable = "CREATE TABLE ";

    private static String insertIntoTable = "insert into ";

    public static <T> String createTable(Class<T> entityClass) {
        createTable += entityClass.getAnnotation(TableName.class).value() + "(\n";
        Field[] classFields = entityClass.getDeclaredFields();
        for (Field field : classFields) {
            final ColumnName columnInfo = field.getAnnotation(ColumnName.class);
            if (!columnInfo.value().equals("")) {
                getFieldInfo(columnInfo, field);
            }
        }
        StringBuilder stringBuilder = new StringBuilder(createTable);
        createTable = stringBuilder.deleteCharAt(stringBuilder.length() - 1).deleteCharAt(stringBuilder.length() - 1).toString();
        String result = createTable + "\n);";
        createTable = "CREATE TABLE ";
        return result;
    }


    public static String insert(Object entity) {
        insertIntoTable += entity.getClass().getAnnotation(TableName.class).value() + "(";
        StringBuilder values = new StringBuilder();
        final Field[] entityFields = entity.getClass().getDeclaredFields();
        for (Field field : entityFields) {
            if (field != null && !field.getAnnotation(ColumnName.class).identity()) {
                insertIntoTable += field.getAnnotation(ColumnName.class).value() + ", ";
                field.setAccessible(true);
                try {
                    if (field.getType().equals(String.class)) {
                        values.append("'").append(field.get(entity)).append("'").append(", ");
                    } else {
                        values.append(field.get(entity)).append(", ");
                    }
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
        StringBuilder stringBuilder = new StringBuilder(insertIntoTable);
        insertIntoTable = stringBuilder.deleteCharAt(stringBuilder.length() - 1).deleteCharAt(stringBuilder.length() - 1) + ") values (";
        values.deleteCharAt(values.length() - 1).deleteCharAt(values.length() - 1);
        String result = insertIntoTable +  values + ");" ;
        insertIntoTable = "insert into ";
        return result;
    }


    private static void getFieldInfo(ColumnName columnInfo, Field field) {
        String fieldType = javaTypeToSQLType(field.getType(), columnInfo);
        if (!columnInfo.value().equals("")) {
            createTable += columnInfo.value() + " " + fieldType;
        }
        if (columnInfo.primary()) {
            createTable += " PRIMARY KEY";
        }
        if (fieldType.equals("BOOLEAN")) {
            if (columnInfo.defaultBoolean()) {
                createTable += " default " + true;
            }
        }
        if (fieldType.equals("VARCHAR")) {
            if (columnInfo.maxLength() != 0) {
                createTable += "(" + columnInfo.maxLength() + ")";
            }
        }
        createTable += ",\n";
    }


    private static String javaTypeToSQLType(Class<?> aClass, ColumnName columnInfo) {
        if (aClass.equals(Integer.class) || aClass.equals(int.class)) {
            if (columnInfo.identity()) {
                return "SERIAL";
            }
            return "INT";
        }
        if (aClass.equals(String.class)) {
            return "VARCHAR";
        }
        if (aClass.equals(Long.class) || aClass.equals(long.class)) {
            if (columnInfo.identity()) {
                return "BIGSERIAL";
            }
            return "BIGINT";
        }
        if (aClass.equals(Boolean.class) || aClass.equals(boolean.class)) {
            return "BOOLEAN";
        }
        if (aClass.equals(Float.class) || aClass.equals(float.class)) {
            return "REAL";
        }
        if (aClass.equals(Double.class) || aClass.equals(double.class)) {
            return "DOUBLE PRECISION";
        }
        if (aClass.equals(Date.class)) {
            return "DATE";
        }
        return "";
    }
}
