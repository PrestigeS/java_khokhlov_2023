package classes;

import annotations.ColumnName;
import annotations.TableName;
    @TableName("account")
    public class User {
        @ColumnName(value = "id", primary = true, identity = true)
        private Long id;
        @ColumnName(value = "first_name", maxLength = 25)
        private String firstName;
        @ColumnName(value = "last_name")
        private String lastName;
        @ColumnName(value = "is_worker", defaultBoolean = true)
        private boolean isWorker;

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public void setWorker(boolean worker) {
            isWorker = worker;
        }
    }
