package ru.itis;

import ru.itis.dto.StudentSignUp;
import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("C:\\Users\\Пользователь\\Desktop\\javaL\\java_khokhlov_2023\\Task05\\03. Database\\resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

        Student student0 = new Student("Bob","Kane",19);
        student0.setId(1L);
        //Find by id
        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
        System.out.println(student);
        //findAllByAgeGreaterThanOrderByIdDesc
        System.out.println("Ищем студентов, которым больше 23: ");
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(23));
        //update
        System.out.println("Update" + "\n" + "Было: " + studentsRepository.findById(student.getId()));
        studentsRepository.update(student0);
        System.out.println("Стало: " + studentsRepository.findById(student.getId()));
        //delete
        Student localStudent = new Student("Dwayne","Johnson",44);
        studentsRepository.save(localStudent);
        System.out.println("Delete: " + "\n" + "Было:" + studentsRepository.findById(localStudent.getId()));
        studentsRepository.delete(localStudent.getId());
        System.out.println("Стало: " + studentsRepository.findById(localStudent.getId()));
    }
}
