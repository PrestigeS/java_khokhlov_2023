drop table if exists "order" ;
drop table if exists client cascade;
drop table if exists driver cascade;
drop table if exists car;
create table client
(
    id         bigserial primary key,
    first_name char(20),
    last_name  char(20),
    email      char(30)
);
create table driver
(
    id         bigserial primary key,
    first_name char(20),
    last_name  char(20)
);
create table car
(
    brand  char(30)
);
create table "order"
(
    id bigserial primary key,
    clientID bigint,
    driverID bigint
);
alter table driver add experience bigint;
alter table driver add car_ID bigint not null default 0;
alter table car add id bigserial primary key;
alter table "order" add price bigint not null default 0;
alter table driver add foreign key (car_ID) references car(id);
alter table "order" add foreign key (clientID) references client(id);
alter table "order" add foreign key (driverID) references driver(id);