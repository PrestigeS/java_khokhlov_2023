insert into client(first_name,last_name,email)
values ('Will','Smith','willSmith@gmail.com'),
       ('Dwayne','Johnson','scala@gmail.com'),
       ('Tom','Cruise','cruise@gmail.com'),
       ('Harry','Styles','1d@gmail.com');
update client
set email = 'newEmail@gmail.com'
where id = 4;
insert into car(brand)
values ('Lada'),('Renault'),('Mercedes'),('Toyota');
insert into driver(first_name, last_name,experience,car_id)
values ('Mike','Vazovskiy',15,1),
       ('Harry','Osborne',20,2),
       ('Freddy','Packet',3,3),
       ('Sam','Johnson',30,4);
update driver
    set experience = 21
where id = 3;
insert into "order"(clientid, driverid,price)
values (1,3,10),(2,1,30),(3,2,120),(4,4,12);
update "order"
set price = 12
where id = 2