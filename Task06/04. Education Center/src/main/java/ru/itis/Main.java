package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcTemplateImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .email("email")
                .password("qwerty0")
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        System.out.println(student);
        //update
        Student testStudent = Student.builder().firstName("Bill").lastName("Terner").age(23).email("bill@gmail.com").password("ads").build();
        testStudent.setId(1L);
        System.out.println("Update" + "\n" + "До: " + studentsRepository.findById(testStudent.getId()));
        studentsRepository.update(testStudent);
        System.out.println("После: " + studentsRepository.findById(testStudent.getId()));
        //delete
        Student localStudent = Student.builder().firstName("Dwayne").lastName("Johnson").age(44).email("newEmail@gmail.com").password("qwerty").build();
        studentsRepository.save(localStudent);
        System.out.println("Delete" + "\n" + "До: " + studentsRepository.findById(localStudent.getId()));
        studentsRepository.delete(localStudent.getId());
        System.out.println("После: " + studentsRepository.findById(localStudent.getId()));
        //greater than
        System.out.println("Больше чем 23: ");
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(23));
    }
}
