package services;

import models.Product;

import java.util.List;

public interface ProductService {
    void add(String name, int quantity, String color, String producer);

    List<Product> findAll();

    boolean deleteById(Long id);
}
