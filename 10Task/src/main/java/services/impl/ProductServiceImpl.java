package services.impl;

import java.util.List;
import models.Product;
import repositories.ProductsRepository;
import services.ProductService;

public class ProductServiceImpl implements ProductService {
    private final ProductsRepository productsRepository;

    public void add(String name, int quantity, String color, String producer) {
        this.productsRepository.save(Product.builder().name(name).quantity(quantity).color(color).producer(producer).build());
    }

    public List<Product> findAll() {
        return this.productsRepository.findAll();
    }

    public boolean deleteById(Long id) {
        return this.productsRepository.deleteById(id);
    }

    public ProductServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }
}
