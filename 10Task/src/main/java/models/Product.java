package models;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Product {
    private Long id;
    private String name;
    private int quantity;
    private String color;
    private String producer;

    public Product(String name, int quantity, String color, String producer) {
        this.name = name;
        this.quantity = quantity;
        this.color = color;
        this.producer = producer;
    }

    public String toString() {
        return this.id + ". Название: " + this.name + ", количество: " + this.quantity + ", цвет: " + this.color + ", производитель: " + this.producer + ";";
    }
}