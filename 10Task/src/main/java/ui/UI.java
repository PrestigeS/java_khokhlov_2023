package ui;

import services.ProductService;

import java.util.Scanner;

public class UI {
    private final Scanner scanner;
    private final ProductService productService;

    public UI(ProductService productService) {
        this.scanner = new Scanner(System.in);
        this.productService = productService;
    }

    public void start() {
        while (true) {
            printMenu();
            String choice = this.scanner.next();
            switch (choice) {
                case "1" -> {

                    System.out.println("Введите: а) название, б) количество, в) цвет, г) производителя");
                    String name = this.scanner.next();
                    int quantity = this.scanner.nextInt();
                    String color = this.scanner.next();
                    String producer = this.scanner.next();
                    productService.add(name, quantity, color, producer);
                    System.out.println("\u001b[0;32mТовар добавлен.\u001b[0m");
                }
                case "2" -> System.out.println(this.productService.findAll());
                case "3" -> {
                    System.out.println("Введите ID товара: ");
                    Long id = this.scanner.nextLong();
                    if (this.productService.deleteById(id)) {
                        System.out.println("\u001b[0;32mТовар удален.\u001b[0m");
                    } else {
                        System.out.println("\u001b[0;31mТовара с таким ID нет.\u001b[0m");
                    }
                }
                case "4" -> System.exit(0);
                default ->
                    System.out.println("\u001b[0;31mТакой команды нет!\u001b[0m");
            }
        }
    }

    private void printMenu() {
        System.out.println("Выберите действие: ");
        System.out.println("1. Добавить товар.");
        System.out.println("2. Посмотреть список товаров.");
        System.out.println("3. Удалить товар по id.");
        System.out.println("4. Выход.");
    }
}
