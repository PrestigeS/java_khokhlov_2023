<%@ page import="ru.itis.models.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<form method="post" action="/app/imageProfile" enctype="multipart/form-data">
    <button type="submit">Сохранить</button>
    <input type="file" name="image" placeholder="Выберите файл...">
</form>

<img src="/app/images/" class="img">

<%String login = ((User) request.getSession().getAttribute("user")).getLogin();%>
<span class="login"><%=login%></span>

<form action="/app/"><button class="btn"
>В меню</button></form>
</body>

<style>
    .img {
        position: absolute;
        left: 40px;
        top: 75px;
        border-radius: 50%;
        width: 250px;
        height: 250px;
        background-size: cover;
    }

    .login{
        position: absolute;
        left: 350px;
        top: 80px;
        font-family: "Roboto Light";
        font-size: 25px;
        font-weight: bold;
    }

    .btn{
        position: absolute;
        left: 44%;
        top: 60%;
        width: 70px;
        height: 30px;
    }
    .btn:hover{
        opacity: 0.7;
    }
    .btn:focus{
        left: 44.5%;
        top: 59.5%;
    }
</style>
</html>
