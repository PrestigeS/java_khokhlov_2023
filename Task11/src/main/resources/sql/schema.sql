drop table if exists product;

-- создание таблицы
create table product
(
    id bigserial primary key,
    name varchar(20),
    color varchar(20),
    quantity bigint,
    producer varchar(30)
);

drop table if exists users;

create table users(
    id bigserial primary key,
    age integer default 0,
    login varchar(30),
    password varchar(30),
    first_name varchar(40) default 'без имени',
    last_name varchar(40) default 'без фамилии',
    avatar_name varchar(100)
);
