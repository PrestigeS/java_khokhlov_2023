package ru.itis.utils;

import ru.itis.models.Product;

import java.util.List;

public class HtmlCreator {
    public static String createHtmlWithTable(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Products</title>\n" +
                "</head>\n" +
                "<body>\n");

        html.append(createProductsTable(products));

        html.append(
                "<form action=\"/app/\">\n" +
                        "    <button class=\"button upd\">В меню</button>\n" +
                        "</form>" +
                        "</body>\n" +
                        "</html>");


        return html.toString();
    }

    public static String createHtmlProfile(List<Product> products) {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Profile</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<form method=\"post\">\n" +
                "<label>\n" +
                "    <input type=\"radio\" name=\"sortBy\" value=\"name\">\n" +
                "    по имени от а до я\n" +
                "</label>\n" +
                "<label>\n" +
                "    <input type=\"radio\" name=\"sortBy\" value=\"color\">\n" +
                "    по цвету от а до я\n" +
                "</label>\n" +
                "<label>\n" +
                "    <input type=\"radio\" name=\"sortBy\" value=\"quantity\">\n" +
                "    по возрастанию кол-ва\n" +
                "</label>\n" +
                "<label>\n" +
                "    <input type=\"radio\" name=\"sortBy\" value=\"producer\">\n" +
                "    по изготовителю от а до я\n" +
                "</label>\n" +
                "    <button type=\"submit\">ok</button>\n" +
                "</form>\n" +
                "<form action=\"/app/\">\n" +
                "    <button>В меню</button>\n" +
                "</form>\n" +
                createProductsTable(products) + "\n" +
                "</body>\n" +
                "</html>";

    }

    private static String createProductsTable(List<Product> products) {
        StringBuilder html = new StringBuilder();
        html.append(
                "<table style=\" border-collapse: collapse; width:40%;\">\n" +
                        "\t<tr style=\"border: 1px solid #dddddd;\">\n" +
                        "\t\t<th style=\"border: 1px solid #dddddd; \">id</th>\n" +
                        "\t\t<th style=\"border: 1px solid #dddddd; \">name</th>\n" +
                        "\t\t<th style=\"border: 1px solid #dddddd; \">quantity</th>\n" +
                        "\t\t<th style=\"border: 1px solid #dddddd; \">color</th>\n" +
                        "\t\t<th style=\"border: 1px solid #dddddd; \">producer</th>\n" +
                        "\t</tr>");
        for (Product product : products) {
            html.append("<tr style=\"border: 1px solid #dddddd;\">\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getName()).append("</td>\n");
            html.append("<td>").append(product.getQuantity()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("<td>").append(product.getProducer()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>");

        return html.toString();
    }
}
