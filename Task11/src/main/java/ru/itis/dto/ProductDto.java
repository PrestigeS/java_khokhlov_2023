package ru.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.models.Product;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private String name;
    private int quantity;
    private String color;
    private String producer;

    public static ProductDto from(Product product){
        ProductDto productDto = new ProductDto();
        productDto.setName(product.getName());
        productDto.setProducer(product.getProducer());
        productDto.setColor(product.getColor());
        productDto.setQuantity(product.getQuantity());
        return productDto;
    }

    public static List<ProductDto> from(List<Product> products){
       return products.stream().map(ProductDto::from).collect(Collectors.toList());
    }
}
