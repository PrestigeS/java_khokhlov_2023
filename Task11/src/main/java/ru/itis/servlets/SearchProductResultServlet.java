package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.models.Product;
import ru.itis.services.products.SearchService;

import java.io.IOException;
import java.util.List;

@WebServlet("/searchResult")
public class SearchProductResultServlet extends HttpServlet {

    private ObjectMapper objectMapper;
    private SearchService searchService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        objectMapper = context.getBean(ObjectMapper.class);
        searchService = context.getBean(SearchService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getParameter("query");
        List<Product> products = searchService.searchProductsByFields(query);
        String jsonResponse = objectMapper.writeValueAsString(products);
        resp.setContentType("application/json");
        resp.getWriter().write(jsonResponse);
    }
}
