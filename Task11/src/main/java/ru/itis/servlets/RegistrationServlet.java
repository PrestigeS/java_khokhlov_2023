package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.models.User;
import ru.itis.repositories.users.UsersRepository;
import ru.itis.security.AuthenticationManager;
import ru.itis.services.users.UsersService;

import java.io.IOException;

import static java.util.Objects.nonNull;

@WebServlet("/signUp")
public class RegistrationServlet extends HttpServlet {
    private UsersService usersService;
    private AuthenticationManager authenticationManager;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext applicationContext = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        usersService = applicationContext.getBean(UsersService.class);
        authenticationManager = applicationContext.getBean(AuthenticationManager.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/html/signUp.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (nonNull(login) && nonNull(password)) {
            if (!authenticationManager.isUserExistsWithLogin(login)) {
                usersService.save(User.builder()
                        .login(login)
                        .password(password)
                        .build());
            }
        }

        resp.sendRedirect("/app/signUp");
    }
}
