package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.services.products.ProductService;
import ru.itis.utils.HtmlCreator;

import java.io.IOException;

@WebServlet(urlPatterns = {"/products", "/findProduct"})
public class AllProductsServlet extends HttpServlet {
    private ProductService productService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("springContext");
        productService = context.getBean(ProductService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        if (req.getRequestURI().equals("/app/products")) {
            String html = HtmlCreator.createHtmlWithTable(productService.findAll());
            resp.getWriter().println(html);
        } else {
            getServletContext().getRequestDispatcher("/html/findProduct.html").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String html;
        if (req.getRequestURI().equals("/app/findProduct")) {
            html = HtmlCreator.createHtmlWithTable(productService.findProductsWithIncompleteParameters(
                    req.getParameter("id"), req.getParameter("name"), req.getParameter("quantity"),
                    req.getParameter("color"), req.getParameter("producer"))
            );
            resp.getWriter().println(html);
        }
        else{
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
