package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.ProductAddFormDto;
import ru.itis.services.products.ProductService;

import java.io.IOException;

@WebServlet(urlPatterns = {"/insert"})
public class InsertProductServlet extends HttpServlet {
    private ProductService productService;
    private ObjectMapper objectMapper;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("springContext");
        productService = context.getBean(ProductService.class);
        objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/html/InsertForm.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = req.getReader().readLine();
        ProductAddFormDto product = objectMapper.readValue(body, ProductAddFormDto.class);
        productService.add(product.getName(), product.getQuantity(), product.getColor(), product.getProducer());

        String jsonResponse = objectMapper.writeValueAsString(productService.findAll());
        resp.setContentType("application/json");
        resp.getWriter().write(jsonResponse);
    }

}
