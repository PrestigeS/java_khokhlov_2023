package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.models.User;
import ru.itis.repositories.users.UsersRepository;
import ru.itis.security.AuthenticationManager;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

import static java.util.Objects.nonNull;

@WebServlet("/signIn")
public class LoginServlet extends HttpServlet {
    private AuthenticationManager manager;
    private UsersRepository usersRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext applicationContext = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        manager = applicationContext.getBean(AuthenticationManager.class);
        usersRepository = applicationContext.getBean(UsersRepository.class);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/html/signIn.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String redirectTo = (String) req.getSession().getAttribute("redirectTo");
        if (nonNull(login) && nonNull(password)) {
            if (manager.authenticate(login, password)) {
                User user = usersRepository.findUserByEmailPassword(login, password).get();
                req.getSession().setAttribute("user", user);
                req.getSession().setAttribute("isAuth", "true");
                if (redirectTo != null && redirectTo.length() > 0 && isUriCorrect(redirectTo)) {
                    req.getSession().getAttribute("redirectTo");
                    resp.sendRedirect(redirectTo);
                } else {
                    resp.sendRedirect("/app/");
                }
            } else {
                resp.sendRedirect("/app/signIn");
            }
        } else {
            resp.sendRedirect("/app/signIn");
        }
    }

    private static boolean isUriCorrect(String uri) {
        Set<String> uris = new HashSet<>();
        uris.add("/app/signIn");
        uris.add("/app/signUp");
        uris.add("/app/profile");
        uris.add("/app/");
        uris.add("/app");
        uris.add("/app/insert");
        uris.add("/app/products");
        uris.add("/app/findProduct");
        uris.add("/app/search");
        uris.add("/app/imageProfile");
        return uris.contains(uri);
    }
}
