package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.models.User;
import ru.itis.repositories.users.UsersRepository;
import ru.itis.repositories.users.UsersRepositoryImpl;
import ru.itis.services.images.ImagesService;
import ru.itis.services.images.ImagesServiceImpl;
import ru.itis.services.users.UsersService;
import ru.itis.services.users.UsersServiceImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

@WebServlet("/images/*")
public class ImagesServlet extends HttpServlet {

    private ImagesService imagesService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("springContext");
        imagesService = context.getBean(ImagesServiceImpl.class);
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        String fileName = user.getFileName();
        resp.setContentType(getServletContext().getMimeType(fileName));
        imagesService.getFile(fileName, resp.getOutputStream());
    }
}
