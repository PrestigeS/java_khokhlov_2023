package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.springframework.context.ApplicationContext;
import ru.itis.models.User;
import ru.itis.repositories.users.UsersRepository;
import ru.itis.repositories.users.UsersRepositoryImpl;
import ru.itis.services.images.ImagesService;
import ru.itis.services.images.ImagesServiceImpl;

import java.io.IOException;

@WebServlet("/imageProfile")
@MultipartConfig
public class ProfileNew extends HttpServlet {
    private ImagesService imagesService;
    private UsersRepository usersRepository;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("springContext");
        imagesService = context.getBean(ImagesServiceImpl.class);
        usersRepository = context.getBean(UsersRepositoryImpl.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/profileNew.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part image = req.getPart("image");
        User user = (User) req.getSession().getAttribute("user");

        if (image.getSize() > 0) {
            String fileName = image.getSubmittedFileName();
            user.setFileName(fileName);
            usersRepository.update(user);
            imagesService.upload(fileName, image.getInputStream());
        }
        resp.sendRedirect("/app/imageProfile");
    }
}
