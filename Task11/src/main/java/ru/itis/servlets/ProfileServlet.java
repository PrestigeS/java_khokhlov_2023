package ru.itis.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.services.products.ProductService;
import ru.itis.utils.HtmlCreator;

import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    private ProductService productService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("springContext");
        productService = context.getBean(ProductService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        String html = "";

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("sortBy")) {
                    html = HtmlCreator.createHtmlProfile(productService.sortProductsByParameter(cookie.getValue()));
                    break;
                } else {
                    html = HtmlCreator.createHtmlProfile(productService.findAll());
                }
            }
        }
        else {
            html = HtmlCreator.createHtmlProfile(productService.findAll());
        }

        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie("sortBy", req.getParameter("sortBy"));
        cookie.setMaxAge(60*60*24*365);
        resp.addCookie(cookie);
        resp.sendRedirect("/app/profile");
    }
}
