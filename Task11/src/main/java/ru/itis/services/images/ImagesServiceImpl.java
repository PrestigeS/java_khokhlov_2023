package ru.itis.services.images;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Service;
import ru.itis.repositories.images.ImagesRepository;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
@RequiredArgsConstructor
public class ImagesServiceImpl implements ImagesService {
    private final ImagesRepository imagesRepository;

    @Override
    public void upload(String fileName, InputStream inputStream) {

        new File("..\\storage\\").mkdirs();
        File file = new File(
                "..\\storage\\"
                        + fileName);
        try {
            Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void getFile(String fileName, OutputStream outputStream) {

        try {
            Files.copy(Paths.get("..\\storage\\" + fileName), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
