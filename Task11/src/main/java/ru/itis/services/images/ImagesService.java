package ru.itis.services.images;

import java.io.InputStream;
import java.io.OutputStream;

public interface ImagesService {
    void upload(String fileName, InputStream inputStream);

    void getFile(String fileName, OutputStream outputStream);

}
