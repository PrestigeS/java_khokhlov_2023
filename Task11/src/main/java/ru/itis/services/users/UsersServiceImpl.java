package ru.itis.services.users;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itis.models.User;
import ru.itis.repositories.users.UsersRepository;

@Component
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService{
    private final UsersRepository usersRepository;
    @Override
    public void save(User user) {
        usersRepository.saveUser(user);
    }

    @Override
    public boolean isUserExistsWithLogin(String login) {
        return usersRepository.isUserExistsWithLogin(login).isPresent();
    }
}
