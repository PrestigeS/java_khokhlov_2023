package ru.itis.services.users;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itis.models.User;


public interface UsersService {
    void save(User user);

    boolean isUserExistsWithLogin(String login);
}
