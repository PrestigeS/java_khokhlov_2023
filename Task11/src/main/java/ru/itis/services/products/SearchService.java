package ru.itis.services.products;

import ru.itis.models.Product;

import java.util.List;

public interface SearchService {
    public List<Product> searchProductsByFields(String query);

}
