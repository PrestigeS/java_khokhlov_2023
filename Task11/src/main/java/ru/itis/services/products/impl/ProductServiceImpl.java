package ru.itis.services.products.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.models.Product;
import ru.itis.repositories.products.ProductsRepository;
import ru.itis.services.products.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductsRepository productsRepository;

    public void add(String name, int quantity, String color, String producer) {
        this.productsRepository.save(Product.builder().name(name).quantity(quantity).color(color).producer(producer).build());
    }

    public List<Product> findAll() {
        return this.productsRepository.findAll();
    }

    public boolean deleteById(Long id) {
        return this.productsRepository.deleteById(id);
    }

    @Override
    public List<Product> findProductsWithIncompleteParameters(String id, String name, String quantity, String color, String producer) {
        List<Product> findProducts = new ArrayList<>();
        List<Product> products = findAll();
        for (Product product : products) {
            if(isExistsProductWithIncompleteParameters(product, id, name, quantity, color, producer)){
                findProducts.add(product);
            }
        }
        return findProducts;
    }

    @Override
    public boolean isExistsProductWithIncompleteParameters(Product product, String id, String name, String quantity, String color, String producer) {
        boolean flag = true;
        if(id.length() > 0 && !Objects.equals(product.getId(), Long.valueOf(id))){
            flag = false;
        }
        if(name.length() > 0 && !product.getName().equals(name)){
            flag = false;
        }
        if(quantity.length() > 0 && product.getQuantity() != Integer.parseInt(quantity)){
            flag = false;
        }
        if(color.length() > 0 && !product.getColor().equals(color)){
            flag = false;
        }
        if(producer.length() > 0 && !product.getProducer().equals(producer)){
            flag = false;
        }

        return flag;
    }

    @Override
    public List<Product> sortProductsByParameter(String sortParameter) {
        return productsRepository.sortProductsByParameter(sortParameter);
    }


}
