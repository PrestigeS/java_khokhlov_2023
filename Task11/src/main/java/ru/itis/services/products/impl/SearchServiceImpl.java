package ru.itis.services.products.impl;

import org.springframework.stereotype.Service;
import ru.itis.models.Product;
import ru.itis.repositories.products.ProductsRepository;
import ru.itis.services.products.SearchService;

import java.util.Collections;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {
    private final ProductsRepository productsRepository;

    public SearchServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public List<Product> searchProductsByFields(String query) {
        if (query == null || query.equals("")) {
            return Collections.emptyList();
        }
        return productsRepository.searchProductsByFields(query);
    }
}
