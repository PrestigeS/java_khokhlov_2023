package ru.itis.listeners;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;

@WebListener
public class SpringConfigServletContextListener implements ServletContextListener {
    private HikariDataSource hikariDataSource;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ApplicationContext context =  new AnnotationConfigApplicationContext(ApplicationConfig.class);
        sce.getServletContext().setAttribute("springContext", context);
        hikariDataSource = context.getBean(HikariDataSource.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        hikariDataSource.close();
    }
}
