package ru.itis.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Product {
    private Long id;
    private String name;
    private int quantity;
    private String color;
    private String producer;

    public String toString() {
        return this.id + ". Название: " + this.name + ", количество: " + this.quantity + ", цвет: " + this.color + ", производитель: " + this.producer + ";";
    }
}