package ru.itis.security;

public interface AuthenticationManager {
    boolean authenticate(String login, String password);

    boolean isUserExistsWithLogin(String login);
}
