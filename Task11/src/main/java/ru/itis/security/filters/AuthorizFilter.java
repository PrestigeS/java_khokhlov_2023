package ru.itis.security.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.repositories.users.UsersRepository;
import ru.itis.security.AuthenticationManager;

import java.io.IOException;

@WebFilter(urlPatterns = {"/*", "/**"})
public class AuthorizFilter extends HttpFilter {


    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        String isAuth = (String) req.getSession().getAttribute("isAuth");
        if (isAuth != null || (req.getRequestURI() != null && (req.getRequestURI().equals("/app/signUp") || req.getRequestURI().equals("/app/signIn")))) {
            chain.doFilter(req, res);
        } else {
            req.getSession().setAttribute("redirectTo", req.getRequestURI());
            res.sendRedirect("/app/signIn");
        }
    }
}
