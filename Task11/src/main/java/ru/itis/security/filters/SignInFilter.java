package ru.itis.security.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebFilter("/signIn")
public class SignInFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String isAuth = (String) req.getSession().getAttribute("isAuth");

        if (isAuth != null && isAuth.length() > 0) {
            res.sendRedirect("/app/profile");
        } else {
            chain.doFilter(req, res);
        }
    }
}
