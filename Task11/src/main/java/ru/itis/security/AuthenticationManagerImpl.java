package ru.itis.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.itis.repositories.users.UsersRepository;

@RequiredArgsConstructor
@Component
public class AuthenticationManagerImpl implements AuthenticationManager {
    private final UsersRepository usersRepository;

    @Override
    public boolean authenticate(String login, String password) {
        return usersRepository.findUserByEmailPassword(login, password).isPresent();
    }

    @Override
    public boolean isUserExistsWithLogin(String login) {
        return usersRepository.isUserExistsWithLogin(login).isPresent();
    }
}
