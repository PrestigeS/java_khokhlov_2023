package ru.itis.repositories.users;

import ru.itis.models.User;

import java.util.Optional;

public interface UsersRepository {
    void saveUser(User user);

    Optional<User> findUserByEmailPassword(String login, String password);

    Optional<User> isUserExistsWithLogin(String login);

    void update(User user);
}
