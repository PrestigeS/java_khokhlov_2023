package ru.itis.repositories.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.itis.models.User;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class UsersRepositoryImpl implements UsersRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UsersRepositoryImpl(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //language=SQL
    private static final String SQL_FIND_USER_BY_LOGIN_PASSWORD = "select * from users where login = :login and password = :password";

    //language=SQL
    private static final String SQL_FIND_USER_BY_LOGIN = "select * from users where login = :login";

    //language=SQL
    private final String SQL_UPDATE_BY_ID = "update users set login = :login," +
            " password = :password, avatar_name = :fileName where id= :id";

    private static final RowMapper<User> studentMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getObject("age", Integer.class))
            .login(row.getString("login"))
            .password(row.getString("password"))
            .fileName(row.getString("avatar_name"))
            .build();

    @Override
    public void saveUser(User user) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("firstName", user.getFirstName());
        parameterSource.addValue("lastName", user.getLastName());
        parameterSource.addValue("age", user.getAge());
        parameterSource.addValue("login", user.getLogin());
        parameterSource.addValue("password", user.getPassword());
        parameterSource.addValue("avatar_name", user.getFileName());

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = jdbcInsert.withTableName("users").usingGeneratedKeyColumns("id").executeAndReturnKey(parameterSource).longValue();
        user.setId(id);
    }

    @Override
    public Optional<User> findUserByEmailPassword(String login, String password) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("login", login);
        sqlParameterSource.addValue("password", password);

        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_FIND_USER_BY_LOGIN_PASSWORD, sqlParameterSource, studentMapper));
        }
        catch (EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> isUserExistsWithLogin(String login) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_FIND_USER_BY_LOGIN,
                    Collections.singletonMap("login", login), studentMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(User user) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("login", user.getLogin());
        params.addValue("password", user.getPassword());
        params.addValue("fileName", user.getFileName());
        params.addValue("id", user.getId());
        namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, params);
    }
}
