package ru.itis.repositories.images;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.FileInfo;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class FileRepositoryImpl implements ImagesRepository{
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public FileRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //language=SQL
    private final static String SQL_SELECT_BY_STORAGE_NAME = "select * from file_info where storage_file_name = :storageFileName";

    private final static RowMapper<FileInfo> fileInfoRowMapper = (row, rowNumber) -> FileInfo.builder()
            .id(row.getLong("id"))
            .originalFileName(row.getString("original_file_name"))
            .storageFileName(row.getString("storage_file_name"))
            .size(row.getLong("size"))
            .mimeType(row.getString("mime_type"))
            .description(row.getString("description"))
            .build();

    @Override
    public void save(FileInfo fileInfo) {
        Map<String, Object> params = new HashMap<>();
        params.put("originalFileName", fileInfo.getOriginalFileName());
        params.put("storageFileName", fileInfo.getStorageFileName());
        params.put("size", fileInfo.getSize());
        params.put("mimeType", fileInfo.getMimeType());
        params.put("description", fileInfo.getDescription());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("file_info")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(params)).longValue();


        fileInfo.setId(id);
    }

    @Override
    public Optional<FileInfo> findByStorageFileName(String fileName) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_STORAGE_NAME,
                    Collections.singletonMap("storageFileName", fileName), fileInfoRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
