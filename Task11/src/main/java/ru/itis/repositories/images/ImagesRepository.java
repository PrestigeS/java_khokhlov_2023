package ru.itis.repositories.images;

import ru.itis.models.FileInfo;

import java.util.Optional;

public interface ImagesRepository {
    void save(FileInfo fileInfo);

    Optional<FileInfo> findByStorageFileName(String fileName);

}
