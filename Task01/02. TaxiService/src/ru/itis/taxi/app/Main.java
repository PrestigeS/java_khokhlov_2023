package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersService.signUp(new SignUpForm("Alex", "Sin",
                "sidikov.marsel@gmail.com", "qwerty007"));

        //Поиск
        System.out.println("Тест поиска");
        UUID userID = UUID.fromString("490372df-1ba7-49fe-b709-c923035787ba");
        System.out.println(usersRepository.findById(userID));

        //Поиск всех
        System.out.println("Найти всех");
        System.out.println(usersRepository.findAll());

        //Удаление по user'у
        System.out.println("Удаление");
        UUID deleteID = UUID.fromString("2c21ff7f-b3f1-49e5-be0e-90fdf9339f1f");
        System.out.println("До: " + usersRepository.findById(deleteID));
        usersRepository.delete(usersRepository.findById(deleteID));
        System.out.println("После: " + usersRepository.findById(deleteID));

//        Удаление по ID
        System.out.println("Удаление по ID");
        System.out.println("До: " + usersRepository.findById(userID));
        usersRepository.deleteById(userID);
        System.out.println("После: " + usersRepository.findById(userID));

        //Update
        System.out.println("Update");
        UUID updateID = UUID.fromString("6aadd4ef-4e4b-41a7-acf3-305bfce43142");
        System.out.println("До: " + usersRepository.findById(updateID));
        usersRepository.update(usersRepository.findById(updateID));
        System.out.println("После: " + usersRepository.findById(updateID));

    }
}
