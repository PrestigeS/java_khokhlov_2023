package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        ArrayList<User> userList = new ArrayList<>();
        try (Scanner scanner = new Scanner(new FileReader(fileName))) {
            while (scanner.hasNext()) {
                String[] userInfo = scanner.next().split("\\|");
                userList.add(new User(UUID.fromString(userInfo[0]), userInfo[1], userInfo[2], userInfo[3], userInfo[4]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return userList;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        ArrayList<User> users = (ArrayList<User>) findAll();
        Iterator<User> iterator = users.iterator();
        if (entity != null) {
            BufferedWriter writer;
            try {
                writer = Files.newBufferedWriter(Paths.get(fileName));
                writer.write("");
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (iterator.hasNext()) {
                User user = iterator.next();
                try (Writer writer2 = new FileWriter(fileName, true);
                     BufferedWriter bufferedWriter = new BufferedWriter(writer2)) {
                    if (!user.getId().equals(entity.getId())) {
                        String userAsString = userToString.apply(user);
                        bufferedWriter.write(userAsString);
                        bufferedWriter.newLine();
                    } else {
                        String userAsString = userToString.apply(entity);
                        bufferedWriter.write(userAsString);
                        bufferedWriter.newLine();
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }


    @Override
    public void delete(User entity) {
        if (entity != null) {
            UUID userID = entity.getId();
            deleteById(userID);
        }
    }

    @Override
    public void deleteById(UUID uuid) {
        ArrayList<User> users = (ArrayList<User>) findAll();
        Iterator<User> iterator = users.iterator();
        BufferedWriter writer;
        try {
            writer = Files.newBufferedWriter(Paths.get(fileName));
            writer.write("");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (iterator.hasNext()) {
            User user = iterator.next();
            try (Writer writer2 = new FileWriter(fileName, true);
                 BufferedWriter bufferedWriter2 = new BufferedWriter(writer2)) {

                if (!user.getId().equals(uuid)) {
                    String userAsString = userToString.apply(user);
                    bufferedWriter2.write(userAsString);
                    bufferedWriter2.newLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public User findById(UUID uuid) {
        try (Scanner scanner = new Scanner(new FileReader(fileName))) {

            while (scanner.hasNext()) {
                String[] userInfo = scanner.next().split("\\|");
                UUID userID = UUID.fromString(userInfo[0]);
                if (userID.equals(uuid)) {
                    return new User(userID, userInfo[1], userInfo[2], userInfo[3], userInfo[4]);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
