--1
select model, speed, hd from PC where price < 500;
--2
select distinct maker from Product where type = 'Printer';
--3
select model, ram, screen from Laptop where price > 1000;
--4
select * from Printer where color = 'y';
--5
select distinct model, speed, hd from PC where (cd = '12x' or cd = '24x') and price < 600;
--6
select distinct a.maker, b.speed from Product a, Laptop b where a.type = 'Laptop' and a.model = b.model and b.hd >=10;
--7
select Product.model, price
from Product join PC on PC.model = Product.model where maker = 'B';
union
select Product.model, price from Product join Laptop on Laptop.model = Product.model where maker = 'B'
union
select Product.model, price from Product join Printer on Printer.model = Product.model where maker = 'B';
--8
select maker from Product where type = 'PC'
except
select maker from Product where type = 'Laptop';
--9
select distinct maker from Product join PC on speed >= 450 where type = 'PC' and PC.model = Product.model;
--10
select model, price  from Printer where price = (select max(price) from Printer);
--11
select avg(speed) from PC;
--12
select avg(speed) from Laptop where price > 1000;
--13
select avg(speed) as avg_speed from PC join Product on Product.model = PC.model where Product.maker = 'A';
--14
select Ships.class, Ships.name, country from Classes join Ships on Classes.numGuns >=10 where Ships.class = Classes.class;
--15
select hd from PC group by hd having count(hd)>=2;
--16
select distinct a.model, b.model, a.speed, a.ram from PC a,
 PC b where a.code > b.code and a.speed = b.speed and a.ram = b.ram and not a.model = b.model;
--17
select distinct a.type, b.model, b.speed from Laptop b, Product a where b.speed < all(select speed from PC) and a.type = 'Laptop';
--18
select distinct Product.maker, price from Printer
join Product on Product.model = Printer.model where Printer.color = 'y'
and price = (select min(price) from Printer where Printer.color = 'y');
--19
select distinct Product.maker, avg(screen) from Laptop join Product on Product.type = 'Laptop'
where Laptop.model = Product.model group by maker, type;
--20
select maker, count(type) from Product where type = 'PC' group by maker, type having count(type)>=3;
--21
select distinct Product.maker, max(price) from PC join Product on Product.type = 'PC' and Product.model = PC.model group by maker;
--22
select speed, avg(price) from PC where speed > 600 group by speed;
--23
select maker from Product join PC on PC.speed >= 750 where Product.model = PC.model
intersect
select maker from Product join Laptop on speed >= 750 where Product.model = Laptop.model;
--24
with  inc_out as(select distinct model, price from PC union all select distinct  model,
price from Laptop union all select distinct model, price from Printer)
select model from inc_out where price = (select max(price) from inc_out);
--25
select distinct maker from Product where model in
(select model from PC where
speed = (select max(speed) from PC where ram = (select min(ram) from PC) ) and ram = (select min(ram) from PC))
and maker in (select maker from Product where type = 'Printer');
--26
with avg_price as (select price from PC join Product on Product.model = PC.model where Product.maker = 'A'
union all
select distinct price from Laptop join Product on Product.model = Laptop.model where Product.maker = 'A')
select avg(price) as AVG_price from avg_price;
--27
select distinct Product.maker, avg(hd) as Avg_hd from PC join Product on Product.model = PC.model
where Product.maker in (select maker from Product where type = 'Printer') group by maker;
--28
select count(maker) as qty from Product where maker in(select maker from Product  group by maker having count(model)=1);
--31
select  distinct class,country from Classes where bore >= 16;
--33
select ship from Outcomes where battle = 'North Atlantic' and result = 'sunk';
--34
select distinct name from Ships join Classes on Ships.class = Classes.class
where displacement > 35000 and Classes.type = 'bb' and Ships.launched >= 1922;
--35
select model, type from product
where model not like '%[^A-Z]%' or model not like '%[^0-9]%';
--36
Select name from Ships where class = name
union select ship as name from classes,outcomes where classes.class = outcomes.ship;
--37
Select a.class from Classes a join (select class, name from Ships
union select Classes.class as cl, Outcomes.ship as name from Outcomes
join Classes on Outcomes.ship = Classes.class) as s On a.class = s.class
group by a.class having count(s.name)=1;
--38
select country from Classes where type = 'bb'
intersect
select country from Classes where type = 'bc';
--39
select distinct a.ship from Outcomes a join Battles b
on a.battle = b.name and a.ship in
(select c.ship from Outcomes c join Battles d
on c.battle = d.name and c.result = 'damaged' and b.date > d.date);
--40
select maker, min(type) from Product group by maker having count(distinct type)=1 and count(model) > 1;
--42
select Outcomes.ship, name from Battles join Outcomes on Outcomes.battle = Battles.name where Outcomes.result = 'sunk';
--44
select name from Ships where name like 'R%'
union
select Ship From Outcomes where Ship like 'R%';
--49
select name from Ships join Classes on Ships.class = Classes.class where Classes.bore = 16
union
select ship from Outcomes join Classes on Classes.class = ship where Classes.bore =16;







