function searchProduct(query) {
    fetch("/liveSearchResult?query=" + query)
        .then((response) => {
            return response.json()
        })
        .then((products) => fillTable(products));
}

function fillTable(products) {
    let table = document.getElementById("table-with-products");
    table.innerHTML = '  <tr>\n' +
        '        <th>Name</th>\n' +
        '        <th>Color</th>\n' +
        '        <th>Quantity</th>\n' +
        '        <th>Producer</th>\n' +
        '    </tr>';

    for (let i = 0; i < products.length; i++) {
        let row = table.insertRow(-1);
        let nameCell = row.insertCell(0)
        let colorCell = row.insertCell(1)
        let quantityCell = row.insertCell(2)
        let producerCell = row.insertCell(3)

        nameCell.innerHTML = products[i].name;
        colorCell.innerHTML = products[i].color;
        quantityCell.innerHTML = products[i].quantity;
        producerCell.innerHTML = products[i].producer;
    }
}

function addProduct(name, color, producer, quantity){
    let body = {
        "name" : name,
        "quantity" : quantity,
        "color" : color,
        "producer" : producer
    };
    document.getElementById("name").value = '';
    document.getElementById("color").value = '';
    document.getElementById("producer").value = '';
    document.getElementById("quantity").value = '';
    fetch("/insert", {
        method : "POST",
        headers : {
            'Content-Type' : "application/json",

        },
        body : JSON.stringify(body)
    })
        .then(response => response.json())
        .then(products => fillTable(products))
        .catch(error => alert(error))
}
