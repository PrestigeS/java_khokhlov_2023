package ru.itis.services.products;

import ru.itis.models.Product;

import java.util.List;

public interface ProductService {
    void add(String name, int quantity, String color, String producer);

    List<Product> findAll();

    boolean deleteById(Long id);

    List<Product> findProductsWithIncompleteParameters(String id, String name, String quantity, String color, String producer);

    boolean isExistsProductWithIncompleteParameters(Product product, String id, String name, String quantity, String color, String producer);

    public List<Product> sortProductsByParameter(String sortParameter);

    public List<Product> searchProductsByFields(String query);
}
