package ru.itis.controllers;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.models.Product;
import ru.itis.services.products.ProductService;

import java.net.http.HttpResponse;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ProductsController {

    private final ProductService productService;

    @GetMapping("/insert")
    public String get(){
        return "insert";
    }

    @PostMapping("/insert")
    @ResponseBody
    public List<Product> add(@RequestBody Product product){
        productService.add(product.getName(), product.getQuantity(), product.getColor(), product.getProducer());
        return productService.findAll();
    }

    @GetMapping("/")
    public String menu(){
        return "menu";
    }

    @GetMapping("/search")
    public String searchPage(Model model){
        model.addAttribute("products", productService.findAll());
        return "defSearch";
    }
    @PostMapping("/search")
    public String search(@RequestParam String id, String name, String quantity, String color, String producer, Model model){
        model.addAttribute("products", productService.findProductsWithIncompleteParameters(id, name, quantity, color, producer));
        return "defSearch";
    }

    @GetMapping("/liveSearch")
    public String liveSearchPage(){
        return "liveSearch";
    }

    @GetMapping("liveSearchResult")
    @ResponseBody
    public List<Product> getResult(@RequestParam String query){
        return productService.searchProductsByFields(query);
    }

    @GetMapping("/profile")
    public String getProfile(Model model, @CookieValue(value = "sortBy", required = false) String sortBy, HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        List<Product> products;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("sortBy")) {
                    products = productService.sortProductsByParameter(sortBy);
                    model.addAttribute("products", products);
                    break;
                } else {
                    products = productService.findAll();
                    model.addAttribute("products", products);
                }
            }
        }
        else {
            products = productService.findAll();
            model.addAttribute("products", products);
        }
        return "profile";
    }
    @PostMapping("/profile")
    public String sort(@RequestParam String sortBy, Model model, HttpServletResponse response){
        Cookie cookie = new Cookie("sortBy", sortBy);
        cookie.setMaxAge(60*60*24*365);
        response.addCookie(cookie);
        model.addAttribute("products", productService.findAll());
        return "redirect:profile";
    }
}
