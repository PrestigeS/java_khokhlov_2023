package ru.itis.dao.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.Product;

import javax.sql.DataSource;
import java.util.*;

@Repository
public class ProductsRepositoryNamedParameterJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private final String SQL_FIND_BY_ID = "select * from product where id =:id";

    //language=SQL
    private final String SQL_DELETE_BY_ID = "delete from product where id = :id";

    //language=SQL
    private final String SQL_UPDATE_BY_ID = "update product set name = :name," +
            " quantity = :quantity, color = :color, producer = :producer where id= :id";

    //language=SQL
    private final String SQL_FIND_ALL = "select * from product order by id";

    //language=SQL
    private final String SQL_FIND_ALL_BY_QUANTITY_GREATER_THAN_ORDER_BY_ID_DESC =
            "select * from product where quantity > :quantity order by id desc ";

    //language=SQL
    private static final String SQL_SEARCH_PRODUCT = "select * from product where name ilike :query or " +
            "       producer ilike :query or color ilike :query";
    
    private static final String SQL_SORT_PRODUCTS_BY = "select * from product";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ProductsRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productMapper = ((row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .color(row.getString("color"))
            .quantity(row.getInt("quantity"))
            .producer(row.getString("producer")).build()
    );

    @Override
    public void save(Product product) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", product.getName());
        params.put("quantity", product.getQuantity());
        params.put("color", product.getColor());
        params.put("producer", product.getProducer());

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = jdbcInsert.withTableName("product").usingGeneratedKeyColumns("id").executeAndReturnKey(new MapSqlParameterSource(params)).longValue();
        product.setId(id);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_FIND_BY_ID, Collections.singletonMap("id", id), productMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void updateById(Product product) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", product.getName());
        params.put("quantity", product.getQuantity());
        params.put("color", product.getColor());
        params.put("producer", product.getProducer());
        params.put("id", product.getId());
        namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, params);
    }

    @Override
    public boolean deleteById(Long id) {
        if (findById(id).isPresent()) {
            namedParameterJdbcTemplate.update(SQL_DELETE_BY_ID, Collections.singletonMap("id", id));
            return true;
        }
        return false;
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_FIND_ALL, productMapper);
    }

    @Override
    public List<Product> findAllByQuantityGreaterThanOrderByIdDesc(int quantity) {
        return namedParameterJdbcTemplate.query(SQL_FIND_ALL_BY_QUANTITY_GREATER_THAN_ORDER_BY_ID_DESC, Collections.singletonMap("quantity", quantity), productMapper);
    }

    @Override
    public List<Product> searchProductsByFields(String query) {
        return namedParameterJdbcTemplate.query(SQL_SEARCH_PRODUCT, Collections.singletonMap("query","%" + query + "%"), productMapper);
    }

    public List<Product> sortProductsByParameter(String sortParameter){
        String sqlQuery = SQL_SORT_PRODUCTS_BY;
        sqlQuery += " order by " + sortParameter;
        return namedParameterJdbcTemplate.query(sqlQuery, Collections.singletonMap("sortParameter", sortParameter), productMapper);

    }
}
