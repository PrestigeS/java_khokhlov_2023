package ru.itis.dao.products;

import ru.itis.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {
    Optional<Product> findById(Long id);

    void updateById(Product product);

    boolean deleteById(Long id);

    List<Product> findAll();

    void save(Product product);

    List<Product> findAllByQuantityGreaterThanOrderByIdDesc(int quantity);

    List<Product> searchProductsByFields(String query);

    public List<Product> sortProductsByParameter(String sortParameter);

}